class Error(Exception):
    """Base class for exceptions in this module."""
    pass

class LevelOutOfRangeError(Error):
    """Exception raised for errors in the input.

    Attributes:
        level -- the level of the scene
    """
    def __init__(self, element, level):
        self.level = level
        self.message = 'LevelOutOfRangeError: level ' + str(level) + ' for ' + element + ' is out of range.'