from datetime import time

from errors import LevelOutOfRangeError
from die import Die
from host import Host

class Scene:
    """Represents a Location with Security and Danger."""

    def __init__(self, level):
        
        # check for valid input for the level of the scene
        if level < 1 or level > 6:
            raise LevelOutOfRangeError('SCENE', level)
        print('A new Scene awakes...')
        self.level = level
        self.log_level()       
        
        self.device_level = self.level
        self.log_devive_level()
        self.maglock_level = self.level
        self.log_maglock_level()

        self.maglocks = []
        self.barriers = []
        self.ambushes = []
        self.covers = []
        self.systems = []

    
    def log_level(self):
        print('LEVEL            [' + str(self.level) + ']')

    def log_devive_level(self):
        print('DEVICE_LEVEL     [' + str(self.device_level) + ']')

    def log_maglock_level(self):
        print('MAGLOCK_LEVEL    [' + str(self.maglock_level) + ']')

    def log_htr_reactiontime(self):
        print('HTR_REACTIONTIME [' + str(self.htr_reactiontime) + ']')

    def roll_htr_reactiontime(self):
        if self.level is 1:
            self.htr_reactiontime = time(Die.roll(2), 0, 0)
            self.log_htr_reactiontime()
        if self.level is 2:
            minutes = Die.roll(1, type='mult', value=10)
            if minutes == 60:
                self.htr_reactiontime = time(1, 0, 0)
            else:
                self.htr_reactiontime = time(0, minutes, 0)
            self.log_htr_reactiontime()
        if self.level is 3:
            self.htr_reactiontime = time(0, Die.roll(1, type='mult', value=5), 0)
            self.log_htr_reactiontime()
        if self.level is 4:
            self.htr_reactiontime = time(0, Die.roll(2, type='mod', value=3), 0)
            self.log_htr_reactiontime()
        if self.level is 5:
            self.htr_reactiontime = time(0, Die.roll(1, type='mod', value=4), 0)
            self.log_htr_reactiontime()
        if self.level is 6:
            self.htr_reactiontime = time(0, Die.roll(1), 0)
            self.log_htr_reactiontime()
    
    def create_host(self, *args, attack=0, sleaze=1, data_processing=2, firewall=3):
        self.host = Host(self, *args, attack=attack, sleaze=sleaze, data_processing=data_processing, firewall=firewall)

    def add_maglock(self, maglock):
        pass
    
    def add_barrier(self, barrier):
        pass

    def add_ambush(self, ambush):
        pass
    
    def add_cover(self, cover):
        pass
    
    def add_system(self, system):
        pass