from errors import LevelOutOfRangeError

class Host:
    def __init__(self, scene, *args, attack=0, sleaze=1, data_processing=2, firewall=3):       # scene, level, attack=0, sleaze=1, data_processing=2, firewall=3
        if len(args) >= 1:
            level = args[0]
            level = int(level)
            if level < 1 or level > 12:
                raise LevelOutOfRangeError('HOST', level)
            self.level = level
        else:
            self.level = scene.level * 2
        
        self.attack = self.level + attack
        self.sleaze = self.level + sleaze
        self.data_processing = self.level + data_processing
        self.firewall = self.level + firewall


        self.log_level()

    def log_level(self):
        print('HOST_LEVEL           [' + str(self.level) + ']')
        print('    ATTACK           [' + str(self.attack) + ']')
        print('    SLEAZE           [' + str(self.sleaze) + ']')
        print('    DATA_PROCESSING  [' + str(self.data_processing) + ']')
        print('    FIREWALL         [' + str(self.firewall) + ']')
