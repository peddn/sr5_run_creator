from random import randint

class Die:
    """Represents a Die. Can do multiple Rolls with Mod."""
    @staticmethod
    def roll(dieCount, type='mod', value=0):
        sum = 0
        for _ in range(dieCount):
            roll = randint(1, 6)
            sum += roll
        if type == 'mod': 
            return sum + value
        if type == 'mult':
            return sum * value