import sys

from errors import LevelOutOfRangeError
from scene import Scene

try:
    #scene = Scene(0, 0)
    #scene = Scene(7, 0)
    #scene = Scene(3, 0)
    #scene = Scene(3, 13)
    #scene = Scene(3)
    scene = Scene(3)
    
    #scene.create_host(6, attack=0, sleaze=1, data_processing=2, firewall=3)
    scene.create_host()
    #scene.create_host(8, attack=3, sleaze=1, data_processing=2, firewall=0)


    scene.roll_htr_reactiontime()
except ValueError as e:
    print(e)
    print('ValueError: input for level was not an integer')
except LevelOutOfRangeError as e:
    print(e.message)
